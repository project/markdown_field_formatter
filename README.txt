CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Markdown Field Formatter module provides a field formatter for file fields, 
it allows to upload markdown files. 
The formatter uses a markdown parser library to display them as valid HTML.

 * For a full description of the module visit:
   https://www.drupal.org/project/markdown_field_formatter

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/markdown_field_formatter


REQUIREMENTS
------------

league/commonmark markdown parser.
https://commonmark.thephpleague.com/


INSTALLATION
------------

  1. Get and install composer if not already installed. https://getcomposer.org/doc/
  2. Install parser with composer:

    composer require league/commonmark ^1.0

  3. Install the module with composer


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > [Content type to
       edit] > Manage display for configuration.
    3. Enable the formatter for the field you need.
    4. Update and save.


MAINTAINERS
-----------

 * Mourad Zitouni (mourad-zitouni) - https://www.drupal.org/u/mourad-zitouni