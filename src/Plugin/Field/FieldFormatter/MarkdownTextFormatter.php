<?php

namespace Drupal\markdown_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use League\CommonMark\CommonMarkConverter;

/**
 * Plugin implementation of the 'markdown_text' formatter.
 *
 * @FieldFormatter(
 *   id = "markdown_text",
 *   label = @Translation("Markdown Text"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class MarkdownTextFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $converter = new CommonMarkConverter([
      'html_input' => 'strip',
      'allow_unsafe_links' => FALSE,
    ]);
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $converter->convert($item->value),
      ];
    }
    return $elements;
  }

}
